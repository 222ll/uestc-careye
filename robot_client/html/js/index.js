function button_ac_click() {
    send_command('/move/ac')
}

function button_cw_click() {
    send_command('/move/cw')
}

function button_control_click() {
    send_command('/move/control')
}
function button_back_click() {
    send_command('/move/back')
}

function button_front_click() {
    send_command('/move/front')
}

function button_left_click() {
    send_command('/move/left')
}

function button_right_click() {
    send_command('/move/right')
}

function button_stop_click() {
    send_command('/move/stop')
}

function button_follow_click() {
    send_command('/move/follow')
}

function send_command(urlpath) {
    $.ajax({
        type : "GET",
        url : urlpath,
        success : function(result) {
            console.log(result);
        },
        error : function(e){
            console.log(e.status);
            console.log(e.responseText);
        }
    })
}

function button_line_click() {
    send_command('/move/line')
}


