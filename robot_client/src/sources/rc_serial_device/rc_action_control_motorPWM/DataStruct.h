//
// Created by Pulsar on 2020/5/5.
//

typedef struct {
    unsigned short wheel_1_speed;
    unsigned short wheel_2_speed;
    unsigned short wheel_3_speed;
    unsigned short sum=0;
    bool weel_2_direction;
    bool weel_1_direction;
    bool weel_3_direction;
}WHEEL_DATA;
