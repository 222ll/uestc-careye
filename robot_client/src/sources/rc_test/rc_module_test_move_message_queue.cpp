//
// Created by Pulsar on 2020/5/19.
//

#include <rc_task/rcMoveTask.h>
#include <iostream>
#include <signal.h>
#include <boost/thread/thread.hpp>
#include <stdio.h>

bool ctrl_c_pressed;

void ctrlc(int) {
    ctrl_c_pressed = true;
    std::cout<<"Exit"<<std::endl;
}
int main(int argc,char **argv){
    signal(SIGINT, ctrlc);
    RC::Task::rc_MoveDevice rcMoveDevice;
    rcMoveDevice.gpio_1 = 402;
    rcMoveDevice.gpio_2 = 405;
    rcMoveDevice.gpio_3 = 431;
    rcMoveDevice.pwm_1 = 1;
    rcMoveDevice.pwm_2 = 3;
    rcMoveDevice.pwm_3 = 0;
    rcMoveDevice.serial_device="/dev/tty";
    boost::thread robot_radar_module(boost::bind(RC::Task::run_move_task, rcMoveDevice));
    getchar();
    robot_radar_module.interrupt();
    robot_radar_module.join();
    getchar();
    return 0;
}