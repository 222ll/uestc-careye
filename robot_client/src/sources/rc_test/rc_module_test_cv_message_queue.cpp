//
// Created by Pulsar on 2020/5/19.
//
#include <rc_task/rcTaskManager.h>
#include <rc_log/rclog.h>
#include <zconf.h>
#include <rc_network/rc_asny_tcp_client.h>
#include <base/slog.hpp>
#include <rc_task/rcMoveTask.h>
#include <rc_task/rcRadarTask.h>
#include <rc_task/rcNetworkTask.h>
#include <rc_task/rcGyroTask.h>
#include <rc_task/rcCVTask.h>
#include <rc_task/rcWebStream.h>
using namespace RC::Task;
int main(int argc,char **argv){
    rc_DeviceInfo rcDeviceInfo;
    rcDeviceInfo.serial_port = "/dev/ttyS5";
    rcDeviceInfo.camera_index = 1;
    rcDeviceInfo.mapping = "./map.bin";
    rc_Thread robot_cv_control_module(boost::bind(run_cv_task, rcDeviceInfo));
    robot_cv_control_module.join();
//    sleep(10);
    return 0;
}