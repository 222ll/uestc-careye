//
// Created by Pulsar on 2020/5/16.
//

#include <rc_cv/rcBodyDetceter.h>
#include <iostream>
#include <base/slog.hpp>
namespace RC {
    namespace CV {
        int BodyDetceter::init_body_cascade(std::string file_path) {
            if (!this->body_cascade.load(file_path)) {
                slog::err<<"Loading face cascade"<<slog::endl;
                return -1;
            } else {
                slog::success<<"Loading face cascade"<<slog::endl;
                return 1;
            }
        }
        std::vector<cv::Rect> BodyDetceter::detcetBody(cv::Mat src) {
            std::vector<cv::Rect> bodies;
            cv::Mat frame_gray;
            cv::cvtColor(src, frame_gray, cv::COLOR_BGR2GRAY);
            equalizeHist(frame_gray, frame_gray);
            this->body_cascade.detectMultiScale(frame_gray, bodies, 1.1, 2, 0, cv::Size(30, 30));
            return bodies;
        }
    }
}
