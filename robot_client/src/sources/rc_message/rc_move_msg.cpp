//
// Created by Pulsar on 2020/5/6.
//

#include <rc_message/rc_move_msg.h>

namespace RC {
    namespace Message {
        MoveMessage::MoveMessage(int max_queue_size) : BaseMessage(max_queue_size) {

        }
        boost::mutex MoveMessage::move_mutex;
    }
}

