//
// Created by Pulsar on 2020/5/16.
//
#include <rc_task/rcTaskManager.h>
#include <iostream>
#include <base/slog.hpp>

namespace RC {
    namespace Task {
        int TaskManager::release() {
            if (this->is_init) {
                if (!this->RobotServer.empty()) {
                    slog::info << "任务数量:" << RobotServer.size() << slog::endl;
                    for (int i = 0; i < RobotServer.size(); i++) {
                        slog::warn << "Service Release: " << RobotServer[i].task_name << slog::endl;
                        RobotServer[i].task_thread->interrupt();
                        RobotServer[i].task_thread->join();
                    }

                } else return false;
            } else {
                slog::err << "You should run init before start" << slog::endl;
                return -1;
            }
            return 0;
        }

        int TaskManager::start_main_task() {
            //自检
            if (this->is_init) {
                if (!this->RobotServer.empty()) {
                    for (int i = 0; i < RobotServer.size(); i++) {
                        slog::warn << "Service: " << RobotServer[i].task_name << slog::endl;
                    }
                } else return false;
            } else {
                slog::err << "You should run init before start" << slog::endl;
                return -1;
            }
            return 0;
        }
    }
}

