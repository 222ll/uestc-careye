//
// Created by PulsarV on 18-5-14.
//
#include <rc_task/rcWebStream.h>
#include <iostream>
#include <base/slog.hpp>
#include <rc_network/rc_httpd_rest.h>

namespace RC {
    namespace Task {
        namespace Httpd{
            boost::shared_ptr<RC::Network::Httpd::RcHttpdRest> rcHttpdRest;
        }
        void run_httpd_task(rc_ServerInfo rcServerInfo) {
            using namespace RC::Network::Httpd;

            slog::info << "WEB任务启动中" << slog::endl;
            int thr = hardware_concurrency();

            slog::info << "CPU核心数 = " << hardware_concurrency() << slog::endl;
            slog::info << "HTTP服务线程 " << thr << " 个" << slog::endl;
            try {
                try {
                    Httpd::rcHttpdRest.reset(new RcHttpdRest(rcServerInfo.httpd_port)) ;
                    Httpd::rcHttpdRest->init(thr);
                    Httpd::rcHttpdRest->start();
                    boost::this_thread::interruption_point();
                }catch (boost::thread_interrupted &e){
                    slog::info << "HTTPD任务关闭" << slog::endl;
                    Httpd::rcHttpdRest->stop();
                }
            } catch (std::runtime_error &e) {
                slog::err << "HTTP启动失败 " << slog::endl;
                slog::err << e.what() << slog::endl;
            }
        }
    }
}

