//
// Created by PulsarV on 18-5-14.
//
#include <rc_task/rcTaskManager.h>
#include <zconf.h>
#include <rc_network/rc_asny_tcp_client.h>
#include <base/slog.hpp>
#include <rc_task/rcMoveTask.h>
#include <rc_task/rcRadarTask.h>
#include <rc_task/rcNetworkTask.h>
#include <rc_task/rcGyroTask.h>
#include <rc_task/rcCVTask.h>
#include <rc_task/rcWebStream.h>

namespace RC {
    namespace Task {

        TaskManager::TaskManager(char *server_name) {
            this->server_name = server_name;
        }

        /**
         * 设备初始化
         * @param camera_index
         * @param serial_port
         * @param mapping
         * @param local_address
         * @param local_port
         * @param remote_address
         * @param remote_port
         */
        void TaskManager::init(int camera_index,//摄像机编号
                               const std::string &serial_port,//控制串口地址
                               int serial_port_freq,//控制串口波特率
                               const std::string &radar_path,//雷达地址
                               const std::string &gyro_path,//陀螺仪地址
                               int gyro_freq,//陀螺仪波特率
                               const std::string &mapping,//地图文件地址
                               int httpd_port,//httpd端口
                               int websocket_port,//websocket端口
                               const std::string &remote_address, //远程服务器地址
                               int remote_port,//远程服务器端口号
                               int gpio_1, int pwm_1,//制动端口号1和PWM端口号1
                               int gpio_2, int pwm_2,//制动端口号2和PWM端口号2
                               int gpio_3, int pwm_3//制动端口号3和PWM端口号3
        ) {


            //启动制动模块
            slog::info << ">>>启动制动模块" << slog::endl;
            slog::info << "gpio_1:" << gpio_1 << " pwm_1:" << pwm_1 << slog::endl;
            slog::info << "gpio_2:" << gpio_2 << " pwm_2:" << pwm_2 << slog::endl;
            slog::info << "gpio_3:" << gpio_3 << " pwm_3:" << pwm_3 << slog::endl;
            rc_MoveDevice rcMoveDevice;
            rcMoveDevice.gpio_1 = gpio_1;
            rcMoveDevice.gpio_2 = gpio_2;
            rcMoveDevice.gpio_3 = gpio_3;
            rcMoveDevice.pwm_1 = pwm_1;
            rcMoveDevice.pwm_2 = pwm_2;
            rcMoveDevice.pwm_3 = pwm_3;
            rcMoveDevice.serial_device = (char *) serial_port.c_str();
            slog::info << "留置控制器串口号:" << serial_port << slog::endl;
//            rc_Thread robot_move_control_module(boost::bind(run_move_task, rcMoveDevice));
            boost::shared_ptr<boost::thread> robot_move_control_module(
                    new boost::thread((boost::bind(run_move_task, rcMoveDevice))));

            this->insert("robot_move_control_module", robot_move_control_module);
//            robot_move_control_module->interrupt();

            //启动视觉模块
            slog::info << ">>>启动视觉模块" << slog::endl;
            slog::info << "摄像机编号:" << camera_index << slog::endl;
            rc_DeviceInfo rcDeviceInfo;
            rcDeviceInfo.camera_index = camera_index;
//            rc_Thread robot_cv_control_module(boost::bind(run_cv_task, rcDeviceInfo));
//            boost::shared_ptr<boost::thread> robot_cv_control_module(new boost::thread((boost::bind(run_cv_task, rcDeviceInfo))));
//            this->insert("robot_cv_control_module",robot_cv_control_module);

            //启动陀螺仪模块
            slog::info << ">>>启动陀螺仪模块" << slog::endl;
            slog::info << "陀螺仪地址:" << gyro_path << slog::endl;
            slog::info << "陀螺仪波特率:" << gyro_freq << slog::endl;
            rc_GyroDevice rcGyroDevice;
            rcGyroDevice.path = gyro_path;
            rcGyroDevice.freq = gyro_freq;
//            rc_Thread robot_gyro_module(boost::bind(run_gyro_task, rcGyroDevice));
//            boost::shared_ptr<boost::thread> robot_gyro_module(new boost::thread((boost::bind(run_gyro_task, rcGyroDevice))));
//            this->insert("robot_gyro_module",robot_gyro_module);

            slog::info << "地图记录路径:" << mapping << slog::endl;
            //启动雷达模块
            slog::info << ">>>启动雷达模块" << slog::endl;
            slog::info << "雷达地址:" << radar_path << slog::endl;
            rc_RadarInfo rcRadarInfo;
            rcRadarInfo.radar_device = (char *) radar_path.c_str();
            rcRadarInfo.mapping = (char *) mapping.c_str();
//            boost::shared_ptr<boost::thread> robot_radar_module(
//                    new boost::thread((boost::bind(run_radar_task, rcRadarInfo))));
//            this->insert("robot_radar_model", robot_radar_module);


            //启动网络模块
            rc_ServerInfo server_info;
            server_info.remote_address = (char *) remote_address.c_str();
            server_info.httpd_port = httpd_port;
            server_info.websocket_port = websocket_port;
            server_info.remote_port = remote_port;

            slog::info << ">>>启动httpd模块" << slog::endl;
            slog::info << "本地端口号:" << httpd_port << slog::endl;
//            rc_Thread robot_httpd_module(boost::bind(run_httpd_task, server_info));
//            boost::shared_ptr<boost::thread> robot_httpd_module(new boost::thread((boost::bind(run_httpd_task, server_info))));
//            this->insert("robot_httpd_module", robot_httpd_module);

            slog::info << ">>>启动websocket模块" << slog::endl;
            slog::info << "本地端口号:" << websocket_port << slog::endl;
//            rc_Thread robot_websocket_module(boost::bind(run_websocket_task, server_info));
//            boost::shared_ptr<boost::thread> robot_websocket_module(new boost::thread((boost::bind(run_websocket_task, server_info))));
//            this->insert("robot_websocket_module", robot_websocket_module);

            slog::info << ">>>启动远程辅助计算模块" << slog::endl;
            slog::info << "远程IP:" << remote_address << slog::endl;
            slog::info << "远程端口号:" << remote_port << slog::endl;
//            rc_Thread robot_network_control_module(boost::bind(run_network_task, server_info));
//            boost::shared_ptr<boost::thread> robot_network_control_module(new boost::thread((boost::bind(run_network_task, server_info))));
//            this->insert("robot_network_control_module", robot_network_control_module);

            this->is_init = true;
        }

        int TaskManager::insert(std::string task_name, boost::shared_ptr<boost::thread> task_thread) {
            rc_TaskInfo taskinfo;
            taskinfo.task_name = task_name;
            taskinfo.task_thread = task_thread;
            this->RobotServer.push_back(taskinfo);
        }
    }
}