#TODO:����������
set(RC_LIB_NAME robocar)
file(GLOB RC_MODULES_SOURCES_DIR ${RC_SOURCES_DIR}/*)
file(GLOB RC_MODULES_HEADERS_DIR ${RC_INCLUDE_DIR})

set(RC_SOURCES "")
set(RC_THIRD_LIBS
        ${TORCH_LIBRARIES}
        ${PYTHON_LIBRARIES}
        ${OpenCV_LIBS}
        ${Boost_LIBRARIES}
        ${PCL_LIBRARIES}
        ${libLAS_LIBRARIES}
        )
foreach (RC_MODULE ${RC_MODULES_SOURCES_DIR})
    message(STATUS "RC_MODULE :${RC_MODULE}")
    file(GLOB RC_MODULE_SOURCES ${RC_MODULE}/*.cpp)
    foreach (RC_MODULE_SOURCE_FILE ${RC_MODULE_SOURCES})
        message(STATUS "RC_MODULE_SOURCE_FILE :${RC_MODULE_SOURCE_FILE}")
        LIST(APPEND RC_SOURCES ${RC_MODULE_SOURCE_FILE})
    endforeach ()
endforeach ()
LIST(LENGTH RC_SOURCES SOURCES_LENGTH)
message(STATUS "SOURCES_LENGTH:${SOURCES_LENGTH}")




add_library(rc_grpc_proto
        ${PROTO_SRC}
        ${PROTO_H})
target_link_libraries(rc_grpc_proto
        libprotobuf
#        pthread
        grpc++
        grpc++_alts
        grpc++_unsecure
        grpc++_error_details
        grpc++_reflection
        )
add_library(${LIB_NAME} SHARED ${RC_SOURCES})
target_link_libraries(${LIB_NAME} ${OpenCV_LIBS}
#        pthread
        ${OPENGL_LIBRARIES}
        ${GLUT_LIBRARY}
        ${PYTHON_LIBRARIES}
        ${OpenCV_LIBS}
        ${Boost_LIBRARIES}
        ${PCL_LIBRARIES}
        glib-2.0
        stdc++
        m
        ${MPI_CXX_LIBRARIES}
        ${realsense2_LIBRARY}
        ${Pistache_LIBRARIES}/libpistache.a
        libserv
        libprotobuf
        rc_grpc_proto
        )
add_dependencies(${LIB_NAME} libprotobuf protoc grpc++)
if (${WITH_LIBREALSENCE2} STREQUAL "OFF")
    target_link_libraries(${LIB_NAME} realsense2)
endif ()
if (${WITH_TBB} STREQUAL "ON")
    target_link_libraries(${LIB_NAME} ${TBB_IMPORTED_TARGETS})
endif ()

