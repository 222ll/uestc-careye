message(STATUS "Load Serial Module")
include_directories(${PROJECT_SOURCE_DIR}/../3rdparty/libserv/src/include)
set(RCSERAIL_DIR ${PROJECT_SOURCE_DIR}/src)
file(GLOB RC_SERAIL_FILES ${RCMAPPING_DIR}/sources/rc_serial/*.cpp)

foreach(FILE ${RC_SERAIL_FILES})
    message(STATUS "RC_SERIAL: ${FILE}")
endforeach()
include_directories(${RCSERAIL_DIR}/include)
