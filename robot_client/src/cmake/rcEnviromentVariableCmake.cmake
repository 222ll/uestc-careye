cmake_minimum_required(VERSION 3.0)


message(STATUS "Author:Pulsar-V")
message(STATUS "Version:1.0.1")



set(MAIN_DIR ${PROJECT_SOURCE_DIR}/src)
message(STATUS "MAIN_DIR is ${MAIN_DIR}")

set(SYSTEM_LIB_DIR /usr/local/libs)
set(USER_LIB_DIR /usr/lib)


if (${CMAKE_CXX_COMPILER_ID} STREQUAL GNU)
	set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall")
endif ()

