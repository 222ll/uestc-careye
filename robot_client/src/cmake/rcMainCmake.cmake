set(EXE_NAME RoboCar)
set(RCMAIN_DIR ${PROJECT_SOURCE_DIR}/src/sources/rc_main)
set(SOURCE_FILES ${RCMAIN_DIR}/main.cpp)
add_executable(${EXE_NAME} ${SOURCE_FILES})
target_link_libraries(${EXE_NAME} ${OpenCV_LIBS}
        robocar
#        pthread
        gflags
        stdc++
        )