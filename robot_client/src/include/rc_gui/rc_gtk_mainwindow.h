//
// Created by PulsarV on 18-5-14.
//

#ifndef ROBOCAR_RC_GTK_MAINWINDOW_H
#define ROBOCAR_RC_GTK_MAINWINDOW_H
#include <opencv/highgui.h>
#include <iostream>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gdkmm-2.4/gdkmm.h>
#include <gtk-2.0/gdk/gdk.h>
#include <gtk-2.0/gtk/gtk.h>
#include <sys/ioctl.h>
#include <rc_gui/rc_gtk_mainwindow.h>
#include "map"
#include <sys/stat.h>
#include <fcntl.h>
#include <linux/fb.h>
#include <xcb/xcb.h>
#include <sys/mman.h>
#include <math.h>
#include <time.h>

namespace RC{
    class rc_gtk_mainwindow {
        rc_gtk_mainwindow();
    };
}


#endif //ROBOCAR_RC_GTK_MAINWINDOW_H
