//
// Created by Pulsar on 2020/5/18.
//

#ifndef UESTC_CAREYE_RC_HTTPD_REST_H
#define UESTC_CAREYE_RC_HTTPD_REST_H

#include <pistache/http.h>
#include <pistache/router.h>
#include <pistache/endpoint.h>
#include <algorithm>
#include <ctime>

namespace RC {
    namespace Network {
        namespace Httpd {
            using namespace std;
            using namespace Pistache;

            class RcHttpdRest {
            public:
                explicit RcHttpdRest(int port);

                void init(size_t thr);

                void start();
                void stop();



            private:
                void setupRoutes();

                void settings(const Rest::Request &request, Http::ResponseWriter response);

                void video(const Rest::Request &request, Http::ResponseWriter response);

                void index(const Rest::Request &request, Http::ResponseWriter response);

                void move(const Rest::Request &request, Http::ResponseWriter response);

                void staticfile(const Rest::Request &request, Http::ResponseWriter response);

                void doAuth(const Rest::Request &request, Http::ResponseWriter response);

                std::shared_ptr<Http::Endpoint> httpEndpoint;
                Rest::Router router;
            };
        }
    }
}


#endif //UESTC_CAREYE_RC_HTTPD_REST_H
