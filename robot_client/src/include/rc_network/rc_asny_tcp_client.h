//
// Created by pulsarv on 19-5-2.
//

#ifndef UESTC_CAREYE_RC_ASNY_TCP_CLIENT_H
#define UESTC_CAREYE_RC_ASNY_TCP_CLIENT_H

#include <string>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/asio.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/enable_shared_from_this.hpp>


namespace RC{
    namespace Network{

        class rc_asny_tcp_client {
        public:
            rc_asny_tcp_client(boost::asio::ip::tcp::endpoint point);
            ~rc_asny_tcp_client();
            bool run();
        private:
            bool connect();
            bool reconnect();
            bool readheart();
            bool sendheart();
            bool conn_handler(const boost::system::error_code&ec, const boost::shared_ptr<boost::asio::ip::tcp::socket>& sock);
            bool read_handler(const boost::system::error_code&ec, boost::shared_ptr<boost::asio::ip::tcp::socket> sock);
        private:
            boost::asio::io_service m_io;
            std::vector<char> m_buf;
            boost::asio::ip::tcp::socket socket;
            boost::asio::ip::tcp::endpoint m_ep;
        };
    }
}

#endif //UESTC_CAREYE_RC_ASNY_TCP_CLIENT_H
