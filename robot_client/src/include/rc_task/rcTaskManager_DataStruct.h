//
// Created by PulsarV on 18-5-14.
//

#ifndef ROBOCAR_RCTASKMANAGER_DATASTRUCT_H
#define ROBOCAR_RCTASKMANAGER_DATASTRUCT_H

#define CAMERA_TYPE int

#include <vector>
#include <boost/thread/thread.hpp>

namespace RC {
    namespace Task {
        typedef struct _rc_TaskInfo{
            std::string task_name;
            boost::shared_ptr<boost::thread> task_thread;
        }rc_TaskInfo;

        typedef std::vector<rc_TaskInfo> rc_ThreadQueue;

        typedef struct _rc_RadarInfo {
            int type=-1;//雷达类型
            char *radar_device{};//雷达地址
            char *mapping;//地图文件地址
        } rc_RadarInfo;

        //视觉设备信息
        typedef struct _rc_DeviceInfo {
            int camera_index;//摄像头编号
        } rc_DeviceInfo;


        //服务器信息
        typedef struct _rc_ServerInfo {
            char *local_address;//本地绑定IP
            int httpd_port;//本地端口号
            int websocket_port;//视频推流端口
            char *remote_address;//远程地址
            int remote_port;//远程端口号
            int port;//
        } rc_ServerInfo;

        //图像数据包
        typedef struct _rc_ImagePackage {
            int ID;
            char imagedata[1024 * 768 * 3];
        } rc_ImagePackage;

        //串口数据包
        typedef struct _rc_SerialPackage {
            char head;
            char sum_h;
            char sum_l;
        } rc_SerialPackage;
        //陀螺仪结构体
        typedef struct _rc_GyroDevice {
            std::string path;
            int freq;
            unsigned char head=0x55;
        } rc_GyroDevice;
        //制动设备信息
        typedef struct _rc_MoveDevice {
            int gpio_1;//制动端口号1
            int pwm_1;//PWM端口号1
            int gpio_2;//制动端口号2
            int pwm_2;//PWM端口号2
            int gpio_3;//制动端口号3
            int pwm_3;//PWM端口号3
            std::string serial_device="";//下位控制器串口号
        } rc_MoveDevice;
    }
}
#endif //ROBOCAR_RCTASKMANAGER_DATASTRUCT_H
