//
// Created by PulsarV on 18-5-14.
//

#ifndef ROBOCAR_RCTASKMANAGER_H
#define ROBOCAR_RCTASKMANAGER_H

#include <map>
#include <rc_move/rcmove.h>
#include <rc_task/rcTaskManager_DataStruct.h>
#include <boost/signal.hpp>
namespace RC {
    namespace Task {
        class TaskManager {
        public:
            explicit TaskManager(char *server_name);

            /**
             *
             * @param camera_index 摄像机编号
             * @param serial_port 控制串口地址
             * @param radar_path 雷达地址
             * @param gyro_path 陀螺仪地址
             * @param mapping 地图文件地址
             * @param local_address 本地绑定IP
             * @param local_port 本地端口
             * @param remote_address 远程服务器地址
             * @param remote_port 远程服务器端口号
             * @param gpio_1 制动端口号1
             * @param pwm_1 PWM端口号1
             * @param gpio_2 制动端口号2
             * @param pwm_2 PWM端口号2
             * @param gpio_3 制动端口号3
             * @param pwm_3 PWM端口号3
             */
            void init(int camera_index,//摄像机编号
                      const std::string &serial_port,//控制串口地址
                      int serial_port_freq,//控制串口波特率
                      const std::string &radar_path,//雷达地址
                      const std::string &gyro_path,//陀螺仪地址
                      int gyro_freq,//陀螺仪波特率
                      const std::string &mapping,//地图文件地址
                      int httpd_port,//httpd端口
                      int websocket_port,//websocket端口
                      const std::string &remote_address, //远程服务器地址
                      int remote_port,//远程服务器端口号
                      int gpio_1, int pwm_1,//制动端口号1和PWM端口号1
                      int gpio_2, int pwm_2,//制动端口号2和PWM端口号2
                      int gpio_3, int pwm_3//制动端口号3和PWM端口号3
            );

            /**
             * 主调度模块
             * @return
             */
            int start_main_task();
            int release();

        private:
            int insert(std::string task_name,boost::shared_ptr<boost::thread> task_thread);

        private:
            bool is_init = false;
            char *server_name;
            rc_ThreadQueue RobotServer;

        };
    }
}
#endif //ROBOCAR_RCTASKMANAGER_H
