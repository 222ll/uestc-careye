//
// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCGYROTASK_H
#define UESTC_CAREYE_RCGYROTASK_H

#include <rc_task/rcTaskManager_DataStruct.h>
#include <string>

namespace RC {
    namespace Task {
        /***
         * 陀螺仪任务
         * @param device_info
         */
        void run_gyro_task(rc_GyroDevice  rcGyroDevice);
    }
}


#endif //UESTC_CAREYE_RCGYROTASK_H
