//
// Created by Pulsar on 2020/5/16.
//

#ifndef UESTC_CAREYE_RCNETWORKTASK_H
#define UESTC_CAREYE_RCNETWORKTASK_H
#include <rc_task/rcTaskManager_DataStruct.h>
namespace RC{
    namespace Task{
        /**
         * 启动网络服务
         * @param device_info
         */
        void run_network_task(rc_ServerInfo server_info);
    }
}


#endif //UESTC_CAREYE_RCNETWORKTASK_H
