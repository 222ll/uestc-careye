//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RCSERIALTASK_H
#define ROBOCAR_RCSERIALTASK_H

#include <rc_task/rcTaskManager_DataStruct.h>
namespace RC{
    namespace Task{
        void run_serial_task(std::string serial_path);
    }
}
#endif //ROBOCAR_RCSERIALTASK_H
