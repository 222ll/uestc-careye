//
// Created by PulsarV on 18-5-9.
//

#ifndef ROBOCAR_RCMOVE_H
#define ROBOCAR_RCMOVE_H

#include <rc_cv/rcCV.h>
#include <rc_log/rclog.h>
#include <rc_serial/rcserial.h>
#include <rc_globalVarable/rc_global_wheel.h>
#include <rc_move/rcmove_data_struct.h>
#include <rc_task/rcTaskManager_DataStruct.h>
#include <libserv/libserv.h>
namespace RC {
    class RobotCarMove {
    public:
        /**
         * 初始化车轮
         * @param moveDevice
         */
        void init(const RC::Task::rc_MoveDevice &moveDevice);

        /**
         * 直接命令
         * @param com
         * @param speed_1
         * @param speed_2
         * @param speed_3
         */
        void command(char com, int speed_1, int speed_2, int speed_3);

        void excute(WHEEL_DATA wheelData);

        void wheel_1_forward(int speed);

        void wheel_1_backward(int speed);

        void wheel_2_forward(int speed);

        void wheel_2_backward(int speed);

        void wheel_3_forward(int speed);

        void wheel_3_backward(int speed);

        /**
         * 前进
         * @param speed_1 1轮轮速
         * @param speed_2 2轮轮速
         */
        void wheel_go_forward(int speed_1, int speed_2);

        /**
         * 后退
         * @param speed_1 1轮轮速
         * @param speed_2 2轮轮速
         */
        void wheel_go_backward(int speed_1, int speed_2);

        /**
         * PWM停止
         */
        void wheel_stop();

        /**
         * PWM开始
         */
        void wheel_start();

        /**
         * 顺时针旋转
         * @param speed_1 1轮轮速
         * @param speed_2 2轮轮速
         * @param speed_3 3轮轮速
         */
        void wheel_CW(int speed_1, int speed_2, int speed_3);

        /**
         * 逆时针旋转
         * @param speed_1 1轮轮速
         * @param speed_2 2轮轮速
         * @param speed_3 3轮轮速
         */
        void wheel_AC(int speed_1, int speed_2, int speed_3);

        /**
         * 陀螺仪校准
         */
        void wheel_check(int times);//校速

        //编码控制数据
        char *decode(
                bool weel_1_direction, unsigned short wheel_1_speed,
                bool weel_2_direction, unsigned short wheel_2_speed,
                bool weel_3_direction, unsigned short wheel_3_speed
        );

        ~RobotCarMove();

    private:
        RC::Task::rc_MoveDevice rcMoveDevice;
        int type = 0;
        bool AutoMove = false;
        bool AutoFollow = false;
#ifdef USE_DSP_DEVICE
        libserv::Serial serial_device;
#endif
    };
}


#endif //ROBOCAR_RCMOVE_H
