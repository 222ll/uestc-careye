//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RC_BASE_MSG_H
#define ROBOCAR_RC_BASE_MSG_H

#include <queue>
#include <map>

namespace RC {
    namespace Message {
        template<typename T>
        class BaseMessage {
        public:
            std::queue <T> self_message_queue;
            int max_queue_size;
        public:
            BaseMessage(int max_queue_size) : max_queue_size(max_queue_size) {

            };

            void pop_message() {
                if (self_message_queue.size() != 0)
                    self_message_queue.pop();
            }

            void push_message(T &cell) {
                self_message_queue.push(cell);
                if ((int) self_message_queue.size() > max_queue_size)
                    pop_message();
            }

            T front_message() {
                return self_message_queue.front();
            }

            T back_message() {
                return self_message_queue.back();
            }

            int size_message() {
                return self_message_queue.size();
            }

            void release_message() {
                while (self_message_queue.size() != 0)
                    self_message_queue.pop();
            }
        };
    }
}
#endif //ROBOCAR_RC_BASE_MSG_H
