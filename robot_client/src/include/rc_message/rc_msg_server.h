//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_RM_MSG_SERVER_H
#define ROBOCAR_RM_MSG_SERVER_H

#include <rc_message/rc_serial_msg.h>
#include <rc_message/rc_image_msg.h>
#include <rc_message/rc_net_work_msg.h>
#include <rc_message/rc_move_msg.h>
#include <rc_message/rc_radar_msg.h>
#include <rc_message/rc_gyro_msg.h>

namespace RC{
    namespace Message{
        class MessageServer {
        public:
            static ImageMessage imageMessage;//图像消息
            static SerialMessage serialMessage;//串口消息
            static NetworkMessage networkMessage;//网络消息
            static MoveMessage moveMessage;//移动消息
            static RadarMessage radarMessage;//雷达消息
            static GyroMessage gyroMessage;//雷达消息
        };
    }
}


#endif //ROBOCAR_RM_MSG_SERVER_H
