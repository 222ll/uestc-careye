//
// Created by PulsarV on 18-10-30.
//

#ifndef ROBOCAR_EC_IMAGE_MSG_H
#define ROBOCAR_EC_IMAGE_MSG_H

#include <rc_message/rc_base_msg.hpp>
#include <rc_task/rcTaskManager_DataStruct.h>
#include <map>
#include <string>
#include <boost/thread/mutex.hpp>

namespace RC {
    namespace Message {
        class ImageMessage :
                public BaseMessage<std::string> {
        public:
            ImageMessage(int max_queue_size);
            static boost::mutex image_mutex;
        };
    }
}


#endif //ROBOCAR_EC_IMAGE_MSG_H
