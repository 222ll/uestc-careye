//
// Created by PulsarV on 18-10-25.
//

#ifndef ROBOCAR_SLAM_DEVICES_H
#define ROBOCAR_SLAM_DEVICES_H

#include <string>
#include <vector>
#include <rplidar.h>

namespace RC {
    namespace Map {
        class DOT {
        public:
            double theta, dist;

            DOT(double theta, double dist) {
                this->dist = dist;
                this->theta = theta;
            }
        };

        class SlamDevice {
        public:
            explicit SlamDevice(const std::string &device_path);

            void bind(void(*call_function)(std::vector <DOT>));

            void start_motor();

            void start_scan(bool force, _u32 timeout);

            void stop();

            void start(bool force, _u32 timeout);

            void recive();

            void release();

        private:
            void (*call_function)(std::vector <DOT>);

            rp::standalone::rplidar::RPlidarDriver *rPlidarDriver= nullptr;
            bool is_start = false;
            bool connectSuccess = false;
        };
    }
}


#endif //ROBOCAR_SLAM_DEVICES_H
