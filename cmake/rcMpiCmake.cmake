message(STATUS "FIND OpenMPI")
set(MPI_C_LIBRARIES
        /usr/lib/x86_64-linux-gnu/libmpi.so
        )
set(MPI_CXX_LIBRARIES
        /usr/lib/x86_64-linux-gnu/libmpi.so
        )
find_package(MPI REQUIRED)

set(MPI_C_INCLUDE_PATH /usr/lib/x86_64-linux-gnu/openmpi/include)
set(MPI_CXX_INCLUDE_PATH /usr/lib/x86_64-linux-gnu/openmpi/include)
if (MPI_FOUND)
    include_directories(${MPI_INCLUDE_PATH})
else ()
    message(ERROR "MPI Not Found")
endif (MPI_FOUND)
