#ifndef RADARVIEWER_H
#define RADARVIEWER_H

#include<QOpenGLWidget>


class RadarViewer : public QOpenGLWidget {
Q_OBJECT
public:
    RadarViewer(QWidget *parent);

    ~RadarViewer();

protected:
    void paintEvent(QPaintEvent *e);

    void paintGL();
};

#endif // RADARVIEWER_H
